# The "TripAccountant" android application

  This application was developed as part of an assignment for the module Dynamic
 Web Development at Waterford Institute of Technology.

  The purpose of the application is to help users to keep track of their daily 
expenses during travel.
  The app uses the user's Facebook account in order to retrieve basic 
informations from the user, such as name, e-mail and profile picture. All the 
session management is done via the Facebook API.
  Once logged the user is able to create new trips, with the names of the 
city and the country being visited as well as the date of arrival and number of days 
that he or she will stay in that city.
  The app also allow the user to add new expenses made in a particular trip,
adding informations such as the amount expended, the description of that cost and 
on which trip that cost happened. The app also contain a simple profile page with 
some basic informations about the user retrieved from Facebook.

# The web-service

  All the data the application access is accessed via a web-service, the
application does not store any data locally.

  The web-service was developed using the RESTful approach, implemented 
as a Jersey webapp and deployed on the Tomcat7 container. All the web-service 
infrastructure (Tomcat7 and PostgreSQL database) are currently deployed on the 
cloud using the Amazon EC2 service.

**Web service address:**
http://ec2-54-72-52-160.eu-west-1.compute.amazonaws.com:8080/tripaccountantAPI/

**Git hub repository of the web service:**
https://github.com/guilhermegentil/trip-accountant-api

  The project itself was developed using the Maven project manager. This platform
was chosen instead of the traditional Eclipse to allow a better control over package 
dependencies and deployment, and also to allow the usage of plug-ins, witch facilitates
the deployment on the cloud.

# The database

  For the persistence the application uses a PostgreSQL relational database. 
This database was chosen because it is proven to be one of the best and more 
reliable open source databases available today.

**Application database model**

	*Users table*
	|uuid      |full_name  |email       |
	|----------|-----------|------------|
	|*user_key*|*user_name*|*user_email*|

	*Trips table*
	|uuid      |city       |country     |start_date            |num_of_days             |user_uuid            |
	|----------|-----------|------------|----------------------|------------------------|---------------------|
	|*trip_key*|*user_name*|*user_email*|*first_day_of_trip*   |*total_number_of_days*  |*foreign_key_to_user*|

	*Expenses table*
	|uuid         |description               |amount               |user_uuid             |trip_uuid             |
	|-------------|--------------------------|---------------------|----------------------|----------------------|
	|*expense_key*|*description_of_the_cost* |*the_amount_expended*|*foreign_key_to_user* |*foreign_key_to_trips*|
