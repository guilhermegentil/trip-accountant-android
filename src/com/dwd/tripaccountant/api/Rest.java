package com.dwd.tripaccountant.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public class Rest {

	private static String server;
	private static HttpParams httpParameters;
	private static DefaultHttpClient httpClient;

	private static final String URL = "http://ec2-54-72-52-160.eu-west-1.compute.amazonaws.com:8080/tripaccountantAPI/api";
	private static final String LocalhostURL = "http://192.168.0.101:8080/tripaccountantAPI/api";

	public static void setup() {
		//Rest.server = LocalhostURL;
		Rest.server = URL;
		httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
		HttpConnectionParams.setSoTimeout(httpParameters, 20000);
		httpClient = new DefaultHttpClient(httpParameters);
	}

	private static String getBase() {
		return server;
	}

	public static String get(String url) {
		String result = "";
		try {
			Log.v("Rest", "GET: " + getBase() + url);
			HttpGet getRequest = new HttpGet(getBase() + url);
			getRequest.setHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			result = getResult(response).toString();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String delete(String url) {
		String result = "";
		try {
			Log.v("Rest", "DEL: " + getBase() + url);
			HttpDelete deleteRequest = new HttpDelete(getBase() + url);
			deleteRequest.setHeader("Content-type", "application/json");
			deleteRequest.setHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(deleteRequest);

			result = getResult(response).toString();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String put(String url, String json) {
		String result = "";
		try {
			Log.v("Rest", "PUT: " + getBase() + url);
			String strRequest = getBase() + url;
			HttpPut putRequest = new HttpPut(strRequest);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("accept", "application/json");
			StringEntity s = new StringEntity(json);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");

			putRequest.setEntity(s);

			HttpResponse response = httpClient.execute(putRequest);
			result = getResult(response).toString();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String post(String url, String json) {
		String result = "";
		try {
			Log.v("Rest", "POST: " + getBase() + url);
			String strRequest = getBase() + url;
			HttpPost postRequest = new HttpPost(strRequest);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("accept", "application/json");			
			StringEntity s = new StringEntity(json);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			postRequest.setEntity(s);
			HttpResponse response = httpClient.execute(postRequest);
			result = getResult(response).toString();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	private static StringBuilder getResult(HttpResponse response)
			throws IllegalStateException, IOException {
		StringBuilder result = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())), 1024);
		String output;
		while ((output = br.readLine()) != null)
			result.append(output);

		return result;
	}

	public static void shutDown() {
		httpClient.getConnectionManager().shutdown();
	}
}