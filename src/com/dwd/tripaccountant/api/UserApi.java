package com.dwd.tripaccountant.api;

import java.lang.reflect.Type;

import android.util.Log;

import com.dwd.tripaccountant.models.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class UserApi {

	public static User get(String call) {
		String json = Rest.get(call);
		Type objType = new TypeToken<User>(){}.getType();		
		User u = new Gson().fromJson(json, objType);
		
		if (u != null && u.getUuid() != null)
		    return u;
		else 
		    return null;
	}
	
	public static User insert(String call,User u) {		
		Type objType = new TypeToken<User>(){}.getType();
		String json = new Gson().toJson(u, objType);
		
		String response = Rest.post(call,json);
		
		return new Gson().fromJson(response, User.class);
	}
}
