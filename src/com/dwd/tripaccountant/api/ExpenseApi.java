package com.dwd.tripaccountant.api;

import java.lang.reflect.Type;
import java.util.List;

import com.dwd.tripaccountant.models.Expense;
import com.dwd.tripaccountant.models.Trip;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ExpenseApi {

    public static List<Expense> getAll(String call) {
        String json = Rest.get(call);
        Type collectionType = new TypeToken<List<Expense>>() {
        }.getType();

        return new Gson().fromJson(json, collectionType);
    }

    public static Expense get(String call) {
        String json = Rest.get(call);
        Type objType = new TypeToken<Expense>() {
        }.getType();

        return new Gson().fromJson(json, objType);
    }

    public static String delete(String call) {
        return Rest.delete(call);
    }

    public static Expense insert(String call, Expense e) {
        Type objType = new TypeToken<Expense>() {
        }.getType();
        String json = new Gson().toJson(e, objType);

        return new Gson().fromJson(Rest.post(call, json), Expense.class);
    }
}
