package com.dwd.tripaccountant.api;

import java.lang.reflect.Type;
import java.util.List;

import com.dwd.tripaccountant.models.Trip;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TripApi {

	public static List<Trip> getAll(String call) {
		String json = Rest.get(call);
		Type collectionType = new TypeToken<List<Trip>>() {}.getType();

		return new Gson().fromJson(json, collectionType);
	}
	
	public static Trip get(String call) {
		String json = Rest.get(call);
		Type objType = new TypeToken<Trip>(){}.getType();

		return new Gson().fromJson(json, objType);
	}

	public static String delete(String call) {
		return Rest.delete(call);
	}

	public static Trip insert(String call,Trip t) {		
		Type objType = new TypeToken<Trip>(){}.getType();
		String json = new Gson().toJson(t, objType);
  
		return new Gson().fromJson(Rest.post(call,json), Trip.class);
	}
	
}
