package com.dwd.tripaccountant.models;

import java.util.ArrayList;
import java.util.List;

public class ExpenseGroup {

    public Trip trip;
    public final List<Expense> children = new ArrayList<Expense>();
    
    public ExpenseGroup(Trip t){
        trip = t;
    }
    
    public ExpenseGroup(){}
    
}
