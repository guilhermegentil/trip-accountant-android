package com.dwd.tripaccountant.models;

import java.io.Serializable;

public class User implements Serializable{

	private String uuid;
	private String facebookId;
	private String fullName;
	private String email;
	private String token;

	public User() {}
	
	public User(String facebookId, String fullName, String email){
		this.facebookId = facebookId;
		this.fullName = fullName;
		this.email = email;
	}

	public User(String uuid, String fullName, String email, String token) {
		this.uuid = uuid;
		this.fullName = fullName;
		this.email = email;
		this.token = token;
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid){
		this.uuid = uuid;
	}
	
	public String getFacebookId() {
		return facebookId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
