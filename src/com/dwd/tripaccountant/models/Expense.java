package com.dwd.tripaccountant.models;

public class Expense {

	private String uuid;
	private String description;
	private Trip trip;
	private User user;
	private float amount;
	
	public Expense(){}

	public Expense(User user, Trip trip, String uuid, float amount,
			String description) {
		this.setUser(user);
		this.setTrip(trip);
		this.uuid = uuid;
		this.setAmount(amount);
		this.setDescription(description);
	}

	public String getUuid() {
		return uuid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

}
