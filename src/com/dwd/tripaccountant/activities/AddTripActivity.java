package com.dwd.tripaccountant.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.api.TripApi;
import com.dwd.tripaccountant.models.Trip;
import com.dwd.tripaccountant.utils.Helper;

public class AddTripActivity extends Activity {

    private static final String TAG = "AddTripActivity";

    private EditText cityEditText;
    private EditText countryEditText;
    private NumberPicker numberOfDaysNumberPicker;
    private DatePicker startDateDatePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip);

        cityEditText = (EditText) findViewById(R.id.city_edit_text);
        countryEditText = (EditText) findViewById(R.id.country_edit_text);

        numberOfDaysNumberPicker = (NumberPicker) findViewById(R.id.number_of_days_number_picker);
        numberOfDaysNumberPicker.setMinValue(0);
        numberOfDaysNumberPicker.setMaxValue(100);

        startDateDatePicker = (DatePicker) findViewById(R.id.start_date_picker);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.trip_add_action_menu, menu);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_menu_activity_add_trip_add:
            new CreateNewTripTask(this).execute();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    //////////////////////////////////////////////////////////////////////////////////////
    private class CreateNewTripTask extends AsyncTask<String, Void, Trip> {

        protected ProgressDialog dialog;
        protected Context context;

        protected Trip t;

        public CreateNewTripTask(Context context) {
            Log.d("CreateNewTripTask", "Enter constructor CreateNewTripTask");
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            t = new Trip();
            t.setCity(cityEditText.getText().toString());
            t.setCountry(countryEditText.getText().toString());
            t.setNumOfDays(numberOfDaysNumberPicker.getValue());
            t.setStartDate(Helper.getDateFromDatePicket(startDateDatePicker));
            t.setUser(MainActivity.user);

            this.dialog = new ProgressDialog(context, 1);
            this.dialog.setMessage("Creating a new Trip");
            this.dialog.show();
        }

        @Override
        protected Trip doInBackground(String... params) {
            try {
                return TripApi.insert(Helper.Api.URL_TRIP, t);
            } catch (Exception e) {
                Log.v("ASYNC", "ERROR : " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Trip result) {
            super.onPostExecute(result);

            t = result;

            if (dialog.isShowing())
                dialog.dismiss();

            finish();
        }

    }

}
