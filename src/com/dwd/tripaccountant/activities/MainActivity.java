package com.dwd.tripaccountant.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.api.UserApi;
import com.dwd.tripaccountant.fragments.AddExpenseFragment;
import com.dwd.tripaccountant.fragments.TripFragment;
import com.dwd.tripaccountant.models.Trip;
import com.dwd.tripaccountant.models.User;
import com.dwd.tripaccountant.utils.FacebookSessionManager;
import com.dwd.tripaccountant.utils.Helper;
import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class MainActivity extends FragmentActivity {

    private static final String TAG = "MainActivity";

    private Fragment currentFragment = null;
    private SlidingMenu slidingMenu = null;

    public static User user = new User();
    public List<Trip> trips = new ArrayList<Trip>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Request.newMeRequest(FacebookSessionManager.getActiveSession(), new GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser fbUser, Response response) {
                Log.d(TAG, "ON COMPLETE: " + response.toString());

                user = new User(fbUser.getId(), fbUser.getName(), (String) fbUser.asMap().get(
                        "email"));

                new UpdateOrCreateUserTask().execute();

            }
        }).executeAsync();

        slidingMenu = new SlidingMenu(this);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.40f);
        slidingMenu.setMenu(R.layout.menu);

        if (savedInstanceState == null) {
            currentFragment = new AddExpenseFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, currentFragment).commit();
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "Called: onPrepareOptionsMenu at MainActivity");
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();

        if (currentFragment instanceof TripFragment) {
            inflater.inflate(R.menu.trip_action_menu, menu);
        }
        else if (currentFragment instanceof AddExpenseFragment){
            inflater.inflate(R.menu.expense_add_action_menu, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            slidingMenu.toggle();
            return true;
        case R.id.action_create_trip:
            if (currentFragment instanceof TripFragment) {
                startActivity(new Intent(this, AddTripActivity.class));
            }
            break;
        case R.id.action_menu_fragment_expense_add:
            if (currentFragment instanceof AddExpenseFragment){
                ((AddExpenseFragment)currentFragment).createNewExpense();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    public void logout() {
        Log.d(TAG, "Executing logout");
        FacebookSessionManager.logout();

        // return the user to the login screen
        startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));

        // make sure the user can not access the page after he/she is logged out
        // clear the activity stack
        finish();
    }

    public void switchContent(Fragment fragment) {
        Log.d(TAG, "Called: switchContent at MainActivity");
        currentFragment = fragment;
        Log.d(TAG, "Current fragment: " + currentFragment.toString());

        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment)
                .commit();
        slidingMenu.showContent();
    }

    private void updateUserUUID(String uuid) {
        user.setUuid(uuid);
        if (currentFragment instanceof AddExpenseFragment) {
            ((AddExpenseFragment) currentFragment).populate();
        }
    }

    private class UpdateOrCreateUserTask extends AsyncTask<String, Void, User> {

        User existingUser;

        @Override
        protected User doInBackground(String... params) {
            try {
                existingUser = UserApi.get(Helper.Api.URL_USER + MainActivity.user.getEmail());                
                if (existingUser == null) {                    
                    new CreateUserTask().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(User u) {
            super.onPostExecute(u);
            if (existingUser != null)
                updateUserUUID(existingUser.getUuid());
        }

        private class CreateUserTask extends AsyncTask<String, Void, User> {

            @Override
            protected User doInBackground(String... params) {
                existingUser = UserApi.insert("/user", MainActivity.user);
                return null;
            }

            @Override
            protected void onPostExecute(User u) {
                super.onPostExecute(u);
                if (existingUser != null)
                    updateUserUUID(existingUser.getUuid());
            }
        }
    }

}
