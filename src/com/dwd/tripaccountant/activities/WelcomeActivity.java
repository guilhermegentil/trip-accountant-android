package com.dwd.tripaccountant.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.dwd.tripaccountant.api.Rest;
import com.dwd.tripaccountant.fragments.WelcomeFragment;
import com.facebook.Session;

public class WelcomeActivity extends FragmentActivity {

	private static final String TAG = "WelcomeActivity";
	private WelcomeFragment welcomeFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "Executing onCreate");
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			// Add the fragment on initial activity setup
			welcomeFragment = new WelcomeFragment();
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, welcomeFragment).commit();
		} else {
			// Or set the fragment from restored state info
			welcomeFragment = (WelcomeFragment) getSupportFragmentManager()
					.findFragmentById(android.R.id.content);
		}
		
		Rest.setup();
	}
	
	@Override 
	protected void onResume() {
		Log.d(TAG, "Executing onResume");
		super.onResume();
		// The application resumed and the session is still open
		// this means the user hinted the back button twice
		// so he/she intents to close the application
		if (Session.getActiveSession().isOpened()){
			finish();
		}
	}
	
}
