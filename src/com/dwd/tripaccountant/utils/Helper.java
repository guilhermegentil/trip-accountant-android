package com.dwd.tripaccountant.utils;

import java.util.Calendar;

import android.widget.DatePicker;

public class Helper {

    /**
     * Get the date as milliseconds from a DatePicker
     * 
     * @param datePicker
     * @return
     */
    public static java.util.Date getDateFromDatePicket(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public class Api {

        public static final String URL_USER = "/user/";
        public static final String URL_TRIP = "/trip/";
        public static final String URL_TRIPS = "/trips/";
        public static final String URL_EXPENSE = "/expense/";
        public static final String URL_EXPENSES = "/expenses/";

    }

}
