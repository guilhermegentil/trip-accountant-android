package com.dwd.tripaccountant.utils;

import android.util.Log;

import com.facebook.Session;
import com.facebook.SessionState;

public class FacebookSessionManager {
	
	private static final String TAG = "FacebookSessionManager";
	

	public static Session getActiveSession(){
		return Session.getActiveSession();
	}

	public static Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	public static void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}
	
	public static void logout(){
		getActiveSession().closeAndClearTokenInformation();
	}

}
