package com.dwd.tripaccountant.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;
import com.facebook.widget.ProfilePictureView;

public class ProfileFragment extends Fragment {
	
	private ProfilePictureView profilePictureView;
	private TextView userNameView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		getActivity().invalidateOptionsMenu();

		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
		
		// Find the user's profile picture custom view
		profilePictureView = (ProfilePictureView) rootView.findViewById(R.id.selection_profile_pic);
		profilePictureView.setCropped(true);
		profilePictureView.setProfileId(MainActivity.user.getFacebookId());
		
		// Find the user's name view
		userNameView = (TextView) rootView.findViewById(R.id.selection_user_name);
		userNameView.setText(MainActivity.user.getFullName());
		
	        this.getActivity().setTitle("My Profile");

		return rootView;
	}
	
}
