package com.dwd.tripaccountant.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;
import com.dwd.tripaccountant.api.TripApi;
import com.dwd.tripaccountant.models.Trip;
import com.dwd.tripaccountant.utils.Helper;

public class TripFragment extends Fragment implements OnItemClickListener {
    
    private ListView listView;

    public void populate() {
        new GetAllTask(this.getActivity()).execute(Helper.Api.URL_USER
                + MainActivity.user.getUuid() + Helper.Api.URL_TRIPS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().invalidateOptionsMenu();

        View rootView = inflater.inflate(R.layout.fragment_trip, container, false);

        listView = (ListView) rootView.findViewById(R.id.my_trips_list);
        listView.setOnItemClickListener(this);
        
        this.getActivity().setTitle("My Trips");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        populate();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub

    }

    // ////////////////////////////////////////////////////////////////////////////////////////
    private class GetAllTask extends AsyncTask<String, Void, List<Trip>> {

        protected ProgressDialog dialog;
        protected Context context;

        public GetAllTask(Context context) {
            Log.d("GetAllTask", "Enter constructor GetAllTask");
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog = new ProgressDialog(context, 1);
            this.dialog.setMessage("Retrieving Trips List");
            this.dialog.show();
        }

        @Override
        protected List<Trip> doInBackground(String... params) {
            try {
                return (List<Trip>) TripApi.getAll((String) params[0]);
            } catch (Exception e) {
                Log.v("ASYNC", "ERROR : " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Trip> result) {
            super.onPostExecute(result);

            ((MainActivity)context).trips = result;
            TripAdapter adapter = new TripAdapter(context, ((MainActivity)context).trips);
            listView.setAdapter(adapter);

            if (dialog.isShowing())
                dialog.dismiss();
        }
    }
}

class TripAdapter extends ArrayAdapter<Trip> {
    private Context context;
    public List<Trip> trips;

    public TripAdapter(Context context, List<Trip> trips) {
        super(context, R.layout.row_trip, trips);
        this.context = context;
        this.trips = trips;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_trip, parent, false);
        Trip trip = trips.get(position);

        TextView cityView = (TextView) view.findViewById(R.id.row_city);
        TextView countryView = (TextView) view.findViewById(R.id.row_country);

        cityView.setText("" + trip.getCity());
        countryView.setText("" + trip.getCountry());

        return view;
    }

    @Override
    public int getCount() {
        return trips.size();
    }
}