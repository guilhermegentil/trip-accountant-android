package com.dwd.tripaccountant.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;
import com.dwd.tripaccountant.api.ExpenseApi;
import com.dwd.tripaccountant.api.TripApi;
import com.dwd.tripaccountant.models.Expense;
import com.dwd.tripaccountant.models.Trip;
import com.dwd.tripaccountant.utils.Helper;

public class AddExpenseFragment extends Fragment {

    private EditText amountEditText;
    private EditText descriptionEditText;
    private Spinner tripSpinner;

    private List<Trip> trips = new ArrayList<Trip>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().invalidateOptionsMenu();

        View rootView = inflater.inflate(R.layout.fragment_add_expense, container, false);

        amountEditText = (EditText) rootView.findViewById(R.id.amount_edit_text);
        descriptionEditText = (EditText) rootView.findViewById(R.id.description_edit_text);
        tripSpinner = (Spinner) rootView.findViewById(R.id.trip_spinner);
        
        this.getActivity().setTitle("Add Expense");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MainActivity.user.getUuid() != null)
            populate();
    }

    public void populate() {
        new GetAllTask(this.getActivity()).execute(Helper.Api.URL_USER
                + MainActivity.user.getUuid() + Helper.Api.URL_TRIPS);
    }

    public void createNewExpense() {
        if (amountEditText.length() == 0 || descriptionEditText.getText().toString() == "")
            Toast.makeText(this.getActivity(), "Empty fields!", Toast.LENGTH_SHORT).show();
        else
            new CreateNewExpenseTask(this.getActivity()).execute(Helper.Api.URL_EXPENSE);
    }

    // ////////////////////////////////////////////////////////////////////////////////////
    private class GetAllTask extends AsyncTask<String, Void, List<Trip>> {

        protected ProgressDialog dialog;
        protected Context context;

        public GetAllTask(Context context) {
            Log.d("GetAllTask", "Enter constructor GetAllTask");
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog = new ProgressDialog(context, 1);
            this.dialog.setMessage("Retrieving Trips List");
            this.dialog.show();
        }

        @Override
        protected List<Trip> doInBackground(String... params) {
            try {
                return (List<Trip>) TripApi.getAll((String) params[0]);
            } catch (Exception e) {
                Log.v("ASYNC", "ERROR : " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Trip> result) {
            super.onPostExecute(result);

            trips = result;

            ArrayAdapter<Trip> adapter = new ArrayAdapter<Trip>(context,
                    android.R.layout.simple_spinner_item, trips);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            tripSpinner.setAdapter(adapter);

            if (dialog.isShowing())
                dialog.dismiss();
        }
    }

    private class CreateNewExpenseTask extends AsyncTask<String, Void, Expense> {

        protected ProgressDialog dialog;
        protected Context context;

        protected Expense e;

        public CreateNewExpenseTask(Context context) {
            Log.d("CreateNewTripTask", "Enter constructor CreateNewTripTask");
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            e = new Expense();
          
            e.setAmount(Float.parseFloat(amountEditText.getText().toString()));
            e.setDescription(descriptionEditText.getText().toString());
            e.setTrip((Trip) tripSpinner.getSelectedItem());
            e.setUser(MainActivity.user);

            this.dialog = new ProgressDialog(context, 1);
            this.dialog.setMessage("Creating a new Expense");
            this.dialog.show();
        }

        @Override
        protected Expense doInBackground(String... params) {
            try {
                return ExpenseApi.insert(Helper.Api.URL_EXPENSE, e);
            } catch (Exception e) {
                Log.v("ASYNC", "ERROR : " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Expense result) {
            super.onPostExecute(result);

            e = result;

            Toast.makeText(context, "Expense Created!", Toast.LENGTH_SHORT).show();

            amountEditText.setText(null);
            descriptionEditText.setText(null);
            tripSpinner.setSelection(0);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

        }

    }
}
