package com.dwd.tripaccountant.fragments;

import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;
import com.dwd.tripaccountant.utils.FacebookSessionManager;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;

public class WelcomeFragment extends Fragment {

	private static final String TAG = "WelcomeFragment";
	private UiLifecycleHelper uiHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "Executing onCreate");
		super.onCreate(savedInstanceState);
		
		uiHelper = new UiLifecycleHelper(getActivity(), FacebookSessionManager.callback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "Executing onCreateView");
		View view = inflater.inflate(R.layout.fragment_welcome, container, false);

		LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
		
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("basic_info", "email"));

		return view;
	}

	@Override
	public void onResume() {
		Log.d(TAG, "Executing onResume");
		super.onResume();

		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			FacebookSessionManager.onSessionStateChange(session, session.getState(), null);
		}

		if (session.isOpened()){
			// Start next activity passing the values
			startActivity( new Intent(this.getActivity(), MainActivity.class));			
		}
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "Executing onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
}
