package com.dwd.tripaccountant.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;

public class SlidingMenuListFragment extends ListFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SampleAdapter adapter = new SampleAdapter(getActivity());

        adapter.add(new SampleItem("Add Expense", R.drawable.add_expense)); // 0
        adapter.add(new SampleItem("My Trips", R.drawable.my_trips)); // 1
        adapter.add(new SampleItem("My Expenses",R.drawable.my_expenses)); // 2
        adapter.add(new SampleItem("My Profile", R.drawable.profile)); // 3
        adapter.add(new SampleItem("Log out", R.drawable.logout)); // 4

        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(android.widget.ListView l, View v, int position, long id) {
        Fragment nextFragment = null;
        switch (position) {
        case 0:
            nextFragment = new AddExpenseFragment();
            break;
        case 1:
            nextFragment = new TripFragment();
            break;
        case 2:
            nextFragment = new ExpenseFragment();
            break;
        case 3:
            nextFragment = new ProfileFragment();
            break;       
        case 4:
            if (getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) getActivity();
                activity.logout();
            }
            break;
        }

        if (nextFragment != null)
            switchFragment(nextFragment);
    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;

        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.switchContent(fragment);
        }
    }

    private class SampleItem {
        public String tag;
        public int iconRes;

        public SampleItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }
    }

    public class SampleAdapter extends ArrayAdapter<SampleItem> {

        public SampleAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
            }
            ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
            icon.setImageResource(getItem(position).iconRes);
            TextView title = (TextView) convertView.findViewById(R.id.row_title);
            title.setText(getItem(position).tag);

            return convertView;
        }

    }
}
