package com.dwd.tripaccountant.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dwd.tripaccountant.R;
import com.dwd.tripaccountant.activities.MainActivity;
import com.dwd.tripaccountant.api.ExpenseApi;
import com.dwd.tripaccountant.models.Expense;
import com.dwd.tripaccountant.models.ExpenseGroup;
import com.dwd.tripaccountant.models.Trip;
import com.dwd.tripaccountant.utils.Helper;

public class ExpenseFragment extends Fragment implements OnItemClickListener {

    private List<Expense> expenses;
    private SparseArray<ExpenseGroup> groups;
    private ExpandableListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().invalidateOptionsMenu();

        View rootView = inflater.inflate(R.layout.fragment_expense, container, false);

        listView = (ExpandableListView) rootView.findViewById(R.id.listView);

        this.getActivity().setTitle("My Expenses");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        
        expenses = new ArrayList<Expense>();
        groups = new SparseArray<ExpenseGroup>();
        
        new GetAllTask(this.getActivity()).execute(Helper.Api.URL_USER
                + MainActivity.user.getUuid() + Helper.Api.URL_EXPENSES);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub

    }

    // ////////////////////////////////////////////////////////////////////////////////////
    private class GetAllTask extends AsyncTask<String, Void, List<Expense>> {

        protected ProgressDialog dialog;
        protected Context context;

        public GetAllTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog = new ProgressDialog(context, 1);
            this.dialog.setMessage("Retrieving Expenses List");
            this.dialog.show();
        }

        @Override
        protected List<Expense> doInBackground(String... params) {
            try {
                return (List<Expense>) ExpenseApi.getAll((String) params[0]);
            } catch (Exception e) {
                Log.v("ASYNC", "ERROR : " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Expense> result) {
            super.onPostExecute(result);

            expenses = result;
            
            List<String> tripUUID = new ArrayList<String>();
            for (Expense e : expenses){
                tripUUID.add(e.getTrip().getUuid());
            }
            
            Set<String> tripUUIDSet = new TreeSet<String>();
            tripUUIDSet.addAll(tripUUID);

            for (String s : tripUUIDSet){

                // POG: store the sum of expenses
                float total = 0.0f;

                ExpenseGroup exGroup = new ExpenseGroup();
                for (Expense e : expenses) {
                    if (e.getTrip().getUuid().equals(s)) {
                        exGroup.trip = e.getTrip();
                        exGroup.children.add(e);
                        total += e.getAmount();
                    }
                }

                // POG: store the sum of expenses
                exGroup.children.add(new Expense(null, null, null, total, "TOTAL:"));
                groups.append(groups.size(), exGroup);
            }

            ExpenseExpandableListAdapter adapter = new ExpenseExpandableListAdapter(getActivity(),
                    groups);
            listView.setAdapter(adapter);

            if (dialog.isShowing())
                dialog.dismiss();
        }
    }
}

// ///////////////////////////////////////////////////////////////////////////////////////
class ExpenseExpandableListAdapter extends BaseExpandableListAdapter {

    private final SparseArray<ExpenseGroup> groups;
    public LayoutInflater inflater;
    public Activity activity;

    public ExpenseExpandableListAdapter(Activity act, SparseArray<ExpenseGroup> groups) {
        activity = act;
        this.groups = groups;
        inflater = act.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild,
            View convertView, ViewGroup parent) {
        final Expense children = (Expense) getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_details, null);
        }

        TextView text = (TextView) convertView.findViewById(R.id.textView1);
        TextView amount = (TextView) convertView.findViewById(R.id.textView2);

        text.setText(children.getDescription());
        amount.setText(String.valueOf(children.getAmount()));

        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, children.getDescription(), Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
            ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_group, null);
        }

        ExpenseGroup group = (ExpenseGroup) getGroup(groupPosition);

        ((CheckedTextView) convertView).setText(group.trip.getCity());
        ((CheckedTextView) convertView).setChecked(isExpanded);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
